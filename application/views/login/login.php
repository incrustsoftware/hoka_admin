<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HOKA Administration System</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" />
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/css/horizontal-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo frontend_theme_url();?>assets/images/favicon.png" />
  <style type="text/css">
    .field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.fa-fw {
    width: 1.28571429em;
    text-align: center;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
    
  </style>
</head>

<body>
<section>
  
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="main-panel" style="background-image:url('<?php echo base_url();?>themes/images/login-bg-img.jpg')">
        <div class ="logo_image">
          <a href="#">
            <img src="<?php echo base_url();?>/themes/images/hoka_logo.png">
          </a>
        </div>
        <div class="content-wrapper d-flex align-items-center auth px-0">
          <div class="row w-100 mx-0">
            <div class="col-lg-5 mx-auto">
              <div class="auth-form-light text-left py-5 px-4 px-sm-5" >
                <h5>Admin Innlogging</h5>
                <form class="pt-3" action="<?php echo base_url();?>index.php/login/check_login/" 
                  method="POST" novalidate>
                  <div class="form-group login_form_group">
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text border-right-0">
                          <i class="ti-user text-primary"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control form-control-lg border-left-0" id="username" name="username" placeholder="Brukernavn">
                    </div>
                    <p style="font-size: 14px; color: red; margin-top: 5px" id="usernameErr"></p>
                  </div>
                  <div class="form-group login_form_group">
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text border-right-0">
                          <i class="ti-lock text-primary"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control form-control-lg border-left-0" id="password" name="password" placeholder="Passord">
                    </div>
                    <p style="font-size: 14px; color: red; margin-top: 5px" id="passwordErr"></p>
                  </div>
                  <div class="errorMsg">
                  <?php
                    if(isset($error_message))
                    {
                    echo '<div class="error" style = "color:red">'.$error_message.'</div>';
                    } 
                    echo '<div class="error">'.$this->session->flashdata('error_message').'</div>';
                  ?>
                  </div>
                  <div class = "frgt_pw_link">
                    <a data-toggle="modal" href="#myModal1">Glemt passord?</a>
                  </div>
                  <div class="row roleSelectionButtonRow">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-6"> 
                      <input class="btn btn-block btn-primary" type="submit" value="Logg inn" style="border-radius: 10px; margin-bottom: 10px;" onclick="return validateLogin();"> 
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
</section>


    </div>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
  
  <script src="<?php echo frontend_theme_url();?>assets/js/jquery.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="<?php echo frontend_theme_url();?>assets/vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page -->
   <!-- Plugin js for this page-->
  <script src="<?php echo frontend_theme_url();?>assets/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo frontend_theme_url();?>assets/js/off-canvas.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/template.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/settings.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo frontend_theme_url();?>assets/js/dashboard.js"></script>
  <!-- End custom js for this page-->
   <!-- Custom js for this page-->
  <script src="<?php echo frontend_theme_url();?>assets/js/data-table.js"></script>
  <!-- End custom js for this page-->
</body>

</html>



<script language="javascript">

$(".toggle-password").click(function() {
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      $(this).removeClass("fa fa-fw fa-eye-slash field-icon toggle-password");
      $(this).addClass("fa fa-fw fa-eye field-icon toggle-password");
      input.attr("type", "text");
    } else {
      $(this).removeClass("fa fa-fw fa-eye field-icon toggle-password");
      $(this).addClass("fa fa-fw fa-eye-slash field-icon toggle-password");
      input.attr("type", "password");
    }
  });

function validateLogin() {
  var usernameVal = $("#username").val();
  var passwordVal = $('#password').val();
  console.log(usernameVal);
  console.log(passwordVal);
  if (usernameVal.length < 1 ) {
    $('#usernameErr').text('Vennligst skriv inn brukernavnet');
  }
  else{
    $('#usernameErr').text('');
  }

  if (passwordVal.length < 1 ) {
    $('#passwordErr').text('Skriv inn passord');
  }
  else{
    $('#passwordErr').text('');
  }

  if ((passwordVal.length > 1) && (usernameVal.length > 1)) {
    return true;
  }
  
  return false;
}
</script>