
<!-- Custom css for this page-->
<link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/app_css/dashboard.css">

<section class="entry_form_content_display container dashboard_page">

    <!-- Date range picker for new complaints -->
    <div class="card m_t_10">
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <label for="date_range_picker" class="m_t_15">New complaints</label>
                    <!-- Nye raklamasjoner -->
                </div>

                <div class="col-6 stretch-card" id="date_range_picker">
                    <div class="input-group new_complaint_picker input-daterange d-flex align-items-center">
                        <input type="text" class="form-control" value="2012-04-05" placeholder="From date">
                       <!--  <div class="input-group-addon mx-4">to</div> -->
                        <input type="text" class="form-control" value="2012-04-19" placeholder="To date">
                    </div>
                </div>

                <div class="col-4">
                    <button type="button" class="btn btn-primary btn-rounded btn-fw" onclick="generateNewComplaintReport();">Generate report</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Complaints slider -->
    <div class="card m_t_10">
        <div class="card-body">
            <div class="complaint_slider owl-carousel">
                <div onclick="viewProductDetails(1, this);">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(2, this);">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(3, this);">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(4, this);">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div onclick="viewProductDetails(5, this);">
                    <div class="row">
                        <div class="col-12">
                            <div class="row image-grid">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                            <div class="row text-grid">
                                <div class="col-12">
                                    <div class="text-block">
                                        <div>M Napali</div>
                                        <div>1091609 BIAG</div>
                                        <div>F27218E</div>
                                    </div>
                                    <div class="text-block">
                                        <div>Naygard Sport AS</div>
                                        <div>Lilleasgata 4</div>
                                        <div>3340 AMOT</div>
                                    </div>
                                    <div class="text-block">
                                        <div>e-post: nygard@sport1.no</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>

    <!-- Complaint graph -->
    <div class="card m_t_10">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <!-- <h4 class="card-title">Multi line chart</h4> -->
                    <canvas id="linechart-multi"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Close cases report -->
    <div class="card m_t_10">
        <div class="card-body">
            <!-- Date range picker for close cases-->
            <div class="row">
                <div class="col-2">
                    <label for="close_cases_date_range_picker" class="m_t_15">Closed cases</label>
                    <!-- Avsluttede saker -->
                </div>

                <div class="col-6 stretch-card" id="close_cases_date_range_picker">
                    <div class="input-group close_cases_picker input-daterange d-flex align-items-center">
                        <input type="text" class="form-control" value="2012-04-05" placeholder="From date">
                       <!--  <div class="input-group-addon mx-4">to</div> -->
                        <input type="text" class="form-control" value="2012-04-19" placeholder="To date">
                    </div>
                </div>

                <div class="col-3">
                    <button type="button" class="btn btn-primary btn-rounded btn-fw" onclick="generateCloseCasesReport();">Generate report</button>
                </div>

                <div class="col-1">
                    <i class="fa fa-file-pdf-o pdf_icon" onclick="closeCasesPDF();" aria-hidden="true"></i>
                </div>
            </div>

            <!-- Datatable for close cases report -->
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive m_t_30">
                        <!-- <label class="badge badge-success">Closed</label>
                        <label class="badge badge-info">On hold</label>
                        <label class="badge badge-danger">Pending</label> -->
                        <table id="close_cases" class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Forhandler</th>
                                    <th>Link til reklamasjonen</th>
                                    <th>Modell</th>
                                    <th>Produksjon</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>101511</td>
                                    <td>Sport 1 Vinje</td>
                                    <td><a target="__blank" href="https://app.hoka.no/reklamasjon&returnID=101511">https://app.hoka.no/reklamasjon&returnID=101511</td>
                                    <td>1091609</td>
                                   
                                    <td>F27218E1</td>
                                    <td>
                                        <i class="fa fa-check fa_right_check" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>101512</td>
                                    <td>Hank Sport</td>
                                    <td><a target="__blank" href="https://app.hoka.no/reklamasjon&returnID=101512">https://app.hoka.no/reklamasjon&returnID=101512</td>
                                    <td>5453948</td>
                                   
                                    <td>F28985G2</td>
                                    <td>
                                        <i class="fa fa-check fa_right_check" aria-hidden="true"></i>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Popup for view complaint data -->

    <div class="modal fade" id="complaint_modal_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Complaint 102547</h5>
                    <!-- Reklamasjon -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div class="row text-grid" style="padding-left: 0px;padding-bottom: 10px;">
                        <div class="col-3">
                            <div class="text-block">
                                <div>M Napali</div>
                                <div>1091609 BIAG</div>
                                <div>F27218E</div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="text-block">
                                <div><label class="label_margin_bottom_0px">Forhandler</label></div>
                                <div>Naygard Sport AS</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="row image-grid" style="padding:0px;">
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image1.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image2.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image3.jpg')?>">
                                </div>
                                <div class="col-6">
                                    <img class="slider-image" src="<?php echo base_url('uploads/product_complaint_images/image4.jpg')?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m_t_5" id="radio_button_grid">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label radio_label">
                                        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option2">
                                        InviceNo:91021, Line ID:64866, OrderNo:49257, ArticleNo:FW1091609BIAG09, NetPrice:999, Discount1:48.5
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m_t_15 hide" id="message_box_grid">
                        <div class="col-md-12">
                            <div class="message_box">
                                <div>Hei,</div>
                                <div>vi har behandlet reklamasjonssaken og vil kredittere order 49257</div>

                                <div class="m_t_15">med vennlig hilsen</div>
                                <div class="m_t_5">Run AS</div>
                            </div>
                        </div>
                        <div class="col-md-12">
                             <i title="back" class="fa fa-long-arrow-left radio_button_back" onclick="showRadioGrid();"></i>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="accepted_btn" type="button" class="btn btn-success btn-rounded btn-sm" data-dismiss="modal" disabled>Accepted<i class="ti-arrow-circle-right ml-1 arrow_icon"></i></button>
                    <button id="not_accepted_btn" type="button" class="btn btn-danger btn-rounded btn-sm" data-dismiss="modal" disabled>Not Accepted<i class="ti-arrow-circle-right ml-1 arrow_icon"></i></button>
                </div>
            </div>
        </div>
    </div>
                   
</section>

<!-- Custom js for this page-->
<script src="<?php echo frontend_theme_url();?>assets/app_js/dashboard.js"></script>
