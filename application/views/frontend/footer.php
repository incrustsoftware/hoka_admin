<footer class="footer footer_new">
<!--   <div class="row footer_andOn">
    <div class="col-md-4">
      
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-md-6">
          <button type="button" class="btn btn-danger btn-rounded btn-fw btn-lg btn_andOn">AndOn</button>
        </div>
        <div class="col-md-6">
          <button type="button" class="btn btn-success btn-rounded btn-icon btn_andon_count">
            <span>0</span>
          </button>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      
    </div>
    
  </div> -->

<style>
.loader {
border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3053a4;
    width: 176px;
    position: absolute;
    height: 175px;
    right: 18px;
    left: 37%;
    display: inline-block;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}


/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

  <style type="text/css">
    
    input#package_months {
    padding: 8px;
}
input#packageMonth {
    padding: 8px;
}

input#date {
    padding: 8px;
}
input#imrr_data {
    padding: 8px;
}
input#received_date {
    padding: 8px;
}
input#time {
    padding: 8px;
}

input#datefrom {
    padding: 8px;
}

input#dateto {
    padding: 8px;
}

.table td {
    /* text-align: left; */
}
.table th {
   /*  text-align: left; */
}
  </style>
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021 <a href="https://hoka.incrustsoftware.com/index.php/login" target="_blank">Hoka</a></span>
   <!--  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ml-1"></i></span> -->
  </div>
</footer>
		</div>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->

  


  <!-- endinject -->
  <script src="<?php echo frontend_theme_url();?>assets/js/jquery-ui.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/chart.js/Chart.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/owl-carousel-2/owl.carousel.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/jquery.avgrund/jquery.avgrund.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/off-canvas.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/hoverable-collapse.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/template.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/settings.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/todolist.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/owl-carousel.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/moment/moment.min.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

  <!-- Custom js for this page-->
  <script src="<?php echo frontend_theme_url();?>assets/js/alerts.js"></script>
  <script src="<?php echo frontend_theme_url();?>assets/js/avgrund.js"></script>
  <!-- End custom js for this page-->
</body>

</html>

