<!DOCTYPE html>
<html lang="en">
<head>
    <title>HOKA Administration System</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" />
    <!-- endinject -->
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/simple-line-icons/css/simple-line-icons.css">
    <!-- Plugin css for this page -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <!-- End plugin css for this page -->

    <!-- End plugin css for this page -->
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/owl-carousel-2/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/owl-carousel-2/owl.theme.default.min.css">
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/css/horizontal-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo frontend_theme_url();?>assets/images/favicon.png" />

    <!-- Date rang picker -->
    <link rel="stylesheet" href="<?php echo frontend_theme_url();?>assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">

    
    <style type="text/css">
        .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow {
            color: #fff;
        }
    </style>
</head>
<body>
   <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <div class="horizontal-menu">
    <nav class="navbar top-navbar col-lg-12 col-12 p-0 header_background">
      <div class="container header_new">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">

        <a class="logo_image" href="<?php  echo base_url();?>index.php/home_page"><img src="<?php echo base_url();?>/themes/images/hoka_logo.png" alt="logo"/></a>
        </div>
        <div class="infoMsg">
          <?php
            if(isset($message_info))
            {
            echo '<div class="infoMsg" style = "color:red">'.$message_info.'</div>';
            } 
            echo '<div class="infoMsg">'.$this->session->flashdata('message_info').'</div>';
          ?>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

            <ul class="navbar-nav navbar-nav-right firstHeadNavRight">
              
                <?php $username = $this->session->userdata('user_name'); ?>
                <input type="hidden" name="plantIdVal" id="plantIdVal" value="<?php echo $this->session->userdata('user_plant_id');?>">

                <li class="nav-item username_show">
                    
                    <a class="nav-link" href="#"  id="username">
                    <p class="plantname_show_p"><?php echo $this->session->userdata('user_plant_name');?></p>
                    </a>
                </li>
                
                <li class="nav-item username_show">
                    
                    <a class="nav-link" href="#"  id="username">
                    <p class="username_show_p">Welcome</p><p class="username_show_p"><?php echo $this->session->userdata('user_name');?></p>
                    </a>
                </li>

              
                <li class="nav-item nav-profile dropdown">
                    <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
                    <img src="<?php echo base_url();?>/themes/images/user.png" alt="profile"/>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                         
                        <a class="dropdown-item" data-toggle="modal" href="#changePassword"  id="change_password">
                            <i class="icon-user" style="color: #248afd;"></i>
                            Change Password?
                        </a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>logout">
                        <i class="ti-power-off text-primary"></i>
                        Logout
                        </a>
                    </div>
                </li>
            </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
        <span class="ti-menu"></span>
        </button>
        </div>
      </div>
    </nav>
    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="changePassword" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header fg_password_modal">
                    <h4 class="modal-title ">Change password</h4>
                    <button type="button" class="modal_close btn btn-danger btn-sm" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <!-- <form class="pt-3" action="#" 
                  method="POST" novalidate> -->
                <form class="pt-3" action="<?php echo base_url();?>login/change_password/" 
                  method="POST" novalidate>
                    <div class="row modal_form_content">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="current_password" class="control-label" >Current password</label>
                                <input type="password" class="form-control form-control-lg changepw_form_group" id="current_password" name="current_password" placeholder="Current password">
                                <p style="font-size: 12px; color: red" id="currentPasswordErr"></p>
                            </div>
                            <div class="form-group ">
                                <label for="new_password" class="control-label" >New password</label>
                                <input type="password" class="form-control form-control-lg " id="new_password"  name="new_password" placeholder="New password">
                                <p style="font-size: 12px; color: red" id="newPasswordErr"></p>
                            </div>
                            <div class="form-group ">
                                <label for="new_password" class="control-label" >Confirm password</label>
                                <input type="password" class="form-control form-control-lg " id="confirm_password"  name="confirm_password" placeholder="Confirm password">
                                <p style="font-size: 12px; color: red" id="confirmPasswordErr"></p>
                                <p style="font-size: 12px; color: red" id="matchPasswordErr"></p>
                            </div>
                            <div class="errorMsg">
                              <?php
                                if(isset($error_message_change_pw))
                                {
                                echo '<div class="error" style = "color:red">'.$error_message_change_pw.'</div>';
                                } 
                                echo '<div class="error">'.$this->session->flashdata('error_message_change_pw').'</div>';
                              ?>
                            </div>
                            <div class="roleSelectionButtonRow">
                                <input class="btn btn-block btn-primary btn-md" type="submit" value="Save" onclick="return validateChangePassword();"> 
                            </div>
                      </div>
                    </div>
                </form>
              
            </div>
        </div>
    </div>
    <?php 
        $firstSegment =$this->uri->segment(1);
        $thirdSegment =$this->uri->segment(3);
        $secondSegment =$this->uri->segment(2);
        
       
    ?>

    <!-- modal -->
    <!-- <nav class="bottom-navbar">
      <div class="container header_new">
        <ul class="nav page-navigation">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>safety">
                    <img class="prod_menu_img" src="<?php echo base_url();?>/themes/images/helmet.png" alt="prod_img"/>
                    <span class="menu-title">HERRER</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <img class="prod_menu_img" src="<?php echo base_url();?>/themes/images/display.png" alt="display_img"/>
                    <span class="menu-title">DAMER</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <img class="prod_menu_img" src="<?php echo base_url();?>/themes/images/work-time.png" alt="prod_img"/>
                    <span class="menu-title">TEKNOLOGY</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>and_on">
                    <img class="prod_menu_img" src="<?php echo base_url();?>/themes/images/alarm.png" alt="prod_img"/>
                    <span class="menu-title">FORHANDLERE</span>
                </a>
            </li>
           <li class="nav-item">
                <a href="#" class="nav-link">
                    <img class="prod_menu_img" src="<?php echo base_url();?>/themes/images/shield.png" alt="display_img"/>
                    <span class="menu-title">KONTAKT</span>
                </a>
            </li>
        </ul>
      </div>
    </nav> -->
</div>
<!-- partial -->
<div class="container-fluid page-body-wrapper">
      <div class="main-panel_old">

<script src="<?php echo frontend_theme_url();?>assets/js/jquery-3.3.1.js"></script>
<script language="javascript">
$(document).ready(function(){
    });
</script>