<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Customer profile API's controller contains login, profile details API 
 */
class Customer_profile_details extends CI_controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->database();
        $this->load->model('customer_profile_details_model');
        $this->load->library('session');
        $this->load->helper('path');
        $this->load->helper('url');
        $this->load->helper('string');
        $this->load->library('encrypt');
        $this->load->dbforge();
        $this->load->dbutil();
        $this->load->helper('common');
	}

	public function register_complaint_data()
	{
		// required headers
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		$data = json_decode(file_get_contents('php://input'), true);
		$headers = apache_request_headers();
		//var_dump($data);die();
    	if(isset($data['product_details']) && isset($data['dealer_name']) && isset($data['product_images']) && isset($data['article_no']) && isset($data['customer_id'])) 
    	{
			$product_details = $data['product_details'];
			$dealer_name = $data['dealer_name'];
			$product_images = $data['product_images'];
			$article_no = $data['article_no'];
			$customer_id = $data['customer_id'];

			$complaint_data = $this->customer_profile_details_model->register_complaint_data($product_details, $dealer_name, $product_images, $article_no, $customer_id);
			if ($complaint_data != false) 
           	{
           		$message = array(
	                "status"=> true,
	                "code" => 200,
	                "message_code" => "Complaint registered successfully"
	            );
           	} else {
	          $message = array(
	                "status"=> false,
	                "code" => 402,
	                "message_code" => "Complaint registration fail",
	            );
	        }
			
    	} else {
            $message = array(
                "status" => false,
                "code" => 422,
                "message_code" => "Invalid request",
            );
        }

		echo json_encode($message);
	}

	/**
	* This function is used to store complaint data.
	* @param $product_details, $dealer_name, $product_images, $article_no, $customer_id, $address1, $address2, $city, $company, $phone, $zip_code
    * @return JSON object
	*/
	public function register_complaint()
	{
		// required headers
		header('content-type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		$data = json_decode(file_get_contents('php://input'), true);
		$headers = apache_request_headers();
		//var_dump($data);die();
    	if(isset($data['product_details']) && isset($data['dealer_name']) && isset($data['product_images']) && isset($data['article_no']) && isset($data['customer_id']) && isset($data['address1']) && isset($data['address2']) && isset($data['city']) && isset($data['company']) && isset($data['phone']) && isset($data['zip_code'])) 
    	{
			$product_details = $data['product_details'];
			$dealer_name = $data['dealer_name'];
			$product_images = $data['product_images'];
			$article_no = $data['article_no'];
			$customer_id = $data['customer_id'];
			$address1 = $data['address1'];
			$address2 = $data['address2'];
			$city = $data['city'];
			$company = $data['company'];
			$phone = $data['phone'];
			$zip_code = $data['zip_code'];

			$complaint_data = $this->customer_profile_details_model->store_complaint_data($product_details, $dealer_name, $product_images, $article_no, $customer_id, $address1, $address2, $city, $company, $phone, $zip_code);
			if ($complaint_data == false) 
           	{
           		getErrorResponse(['Complaint registration fail.']);
           	}

           	getSuccessResponse('', ['Complaint registered successfully.']);
			
    	} else {
    		getErrorResponse(['Invalid request.']);
        }
	}
}
?>