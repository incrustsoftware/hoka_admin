<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_page extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->library('encrypt');
        $this->load->helper('url');
        $this->load->helper('my_url');
        $this->load->helper('string');
        $this->load->library('session');
        $this->load->helper('path');
        $this->load->library('form_validation');
        $this->load->dbforge();
        $this->load->dbutil();
    }


    public function Checklogin() 
    {
        if($this->session->userdata('username') == '' )
        {
            redirect('index.php/login/');
        }
        
    }

    public function index()
    {   
        $this->Checklogin();
        $this->load->view('frontend/header');
        $data['include'] = 'home_page/home_page';
        $this->load->view('frontend/container',$data);
        $this->load->view('frontend/footer');
    }
}
