<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->library('encrypt');
		$this->load->helper('url');
		$this->load->helper('my_url');
        $this->load->helper('string');
        $this->load->library('session');
        $this->load->helper('path');
        $this->load->library('form_validation');
        $this->load->dbforge();
        $this->load->dbutil();
	}

	public function index()
	{
        $data['include'] = 'login/login';
        $this->load->view('frontend/container',$data);
	}

	public function check_login()
	{
		if(isset($_POST))
		{
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if($this->login_model->check_login($this->input->post('username'),$this->input->post('password')))
			{
				$this->session->set_flashdata('message','Du har logget inn');
				$this->session->set_userdata($data);
		
				redirect('index.php/home_page/');
			}
			else
			{
				$data['error_message'] = 'Ugyldig legitimasjon. Vær så snill, prøv på nytt';
			}
			
		}
		else
		{
			if($this->session->userdata('username') != '')
			{
				redirect('login/index');
			}
		}
		
        $data['include'] = 'login/login';
        $this->load->view('frontend/container',$data);

	}

	public function change_password()
	{
		if(isset($_POST))
		{
			$change_password = $this->login_model->change_password();
			if($change_password == true)
			{
				$this->session->set_flashdata('message_info','Your password has been changed successfully !');
				redirect('home_page/');
			}else{
				$this->session->set_flashdata('message_info','Current password does not match, please try again !');
				redirect('home_page/');

				//$data['error_message_change_pw'] = 'Current password does not match, please try again.';
			}
		}
	}

}
