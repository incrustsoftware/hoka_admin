<?php 


if (!function_exists('getErrorResponse')) {
    function getErrorResponse($message)
    {
        $res = [
            'Status' => false,
            'Message' => $message,
        ];

        $CI =& get_instance();

        return $CI->output->set_status_header(400)
    		->set_content_type('application/json')
    		->set_output(json_encode($res));
    }
}

if (!function_exists('getSuccessResponse')) {
    function getSuccessResponse(
        $data = '',
        $message = []
    ) {

    	$CI =& get_instance();

    	$res = [
            'Status' => true,
            'Data' => $data,
            'Message' => $message,
        ];

    	return $CI->output->set_status_header(200)
    		->set_content_type('application/json')
    		->set_output(json_encode($res));
    }
}

if (!function_exists('getUnauthorizedResponse')) {
    function getUnauthorizedResponse()
    {
        $res = [
            'Status' => false,
            'Message' => ["unauthorized access."],
        ];

        $CI =& get_instance();

        return $CI->output->set_status_header(401)
    		->set_content_type('application/json')
    		->set_output(json_encode($res));
    }
}


?>