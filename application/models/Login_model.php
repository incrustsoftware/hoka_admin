<?php
class Login_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}

	public function check_login($user,$pass)
	{
		$encrypted_pw = md5($pass);
	 	$query = $this->db->query("
	 								SELECT u.id, u.username, u.password, u.name, u.user_image, u.mobile_number, u.email_id, u.password_encrypted
	 								FROM 
	 									admin_users as u 
	 								WHERE  
	 									u.username = '$user' 
	 								AND 
	 									u.password_encrypted = '$encrypted_pw'
	 								AND 
	 								u.deleteflag = 0 ");

		$result = $query->result();
		
		$cnt = count($result);
		if($cnt >= 1)
		{
			$data = array('id' => $result[0]->id, 'username' => $result[0]->username, 'user_password'=> $result[0]->password, 'user_full_name'=> $result[0]->name , 'user_image'=> $result[0]->user_image ,'mobile_number'=> $result[0]->mobile_number, 'email_id'=> $result[0]->email_id, 'password_encrypted' => $result[0]->password_encrypted);
		   	$this->session->set_userdata($data);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		 
	}

	public function change_password()
	{
		$userName = $this->session->userdata('user_name');
		$current_password = $_POST['current_password'];
		$new_password = $_POST['new_password'];
		$confirm_password = $_POST['confirm_password'];
		$query = $this->db->query("SELECT * FROM usermaster where userName = '$userName' ");
		$queryRes = $query->row_array();
		if(md5($current_password) == $queryRes['password'])
		{
			$userId = $queryRes['id'];
			$new_password = md5($new_password);
			$result = $this->db->query("UPDATE 
										usermaster
										SET 
											password = '$new_password'
										WHERE 
											id = '$userId'
										AND 
											statusFlag = 1
									");
			$this->session->set_userdata('user_password', $new_password);
			// var_dump($result);
			// var_dump($this->session->userdata('user_password'));		
			return true;

		}else{
			return false;
		}

	}
	
}