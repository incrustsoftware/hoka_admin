<?php
class Customer_profile_details_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}

	public function register_complaint_data($product_details, $dealer_name, $product_images, $article_no, $customer_id)
	{
		$created_by = $customer_id;
		$updated_by = $customer_id;
	    $now = new DateTime();
	    //$now->setTimezone(new DateTimezone('Asia/Bahrain'));
	    $created_on = $now->format('Y-m-d H:i:sa');
	    $modified_on = $now->format('Y-m-d H:i:sa');

		$data = array(
			'customer_id' => $customer_id,
	        'product_details' => $product_details,
	        'dealer_name'=> $dealer_name,
	        'article_no'=> $article_no,
	        'created_by' => $created_by,
	        'created_at' => $created_on,
	        'updated_by' => $updated_by,
	        'updated_at' => $modified_on
	    );

	    $this->db->insert('product_complaint_data',$data);
	    $complaint_id = $this->db->insert_id();

	    if ($complaint_id !='') 
	    {
	    	if (!empty($product_images)) 
		    {
		    	foreach ($product_images as $product_image_str) 
		    	{
		    		$filePath = '';
			      	if ($product_image_str != '') 
			      	{
			      		$image = base64_decode($product_image_str);
			            $image_name = md5(uniqid(rand(), true));
			            // image name generating with random number with 32 characters
			            $filename = $image_name . '.' . 'png';
			            $path = set_realpath('uploads/product_complaint_images');
			            //image uploading folder path
			            file_put_contents($path . $filename, $image);
			            // image is bind and upload to respective folder
			            $filePath = 'uploads/product_complaint_images/'.$filename;

			            $data_img = array(
			            	'complaint_id' => $complaint_id,
					        'product_iamge' => $filePath,
					        'created_by' => $created_by,
					        'created_at' => $created_on,
					        'updated_by' => $updated_by,
					        'updated_at' => $modified_on
			            );

			            $this->db->insert('product_complaint_images',$data_img);
			      	}
		    	}
		    }
		    return true;
	    }else{
	    	return false;
	    }
	}

	/**
	* This function is used to store complaint data.
	* @param $product_details, $dealer_name, $product_images, $article_no, $customer_id, $address1, $address2, $city, $company, $phone, $zip_code
    * @return JSON object
	*/
	public function store_complaint_data($product_details, $dealer_name, $product_images, $article_no, $customer_id, $address1, $address2, $city, $company, $phone, $zip_code)
	{
		$created_by = $customer_id;
		$updated_by = $customer_id;
	    $now = new DateTime();
	    //$now->setTimezone(new DateTimezone('Asia/Bahrain'));
	    $created_on = $now->format('Y-m-d H:i:sa');
	    $modified_on = $now->format('Y-m-d H:i:sa');

		$data = array(
			'customer_id' => $customer_id,
	        'product_details' => $product_details,
	        'dealer_name'=> $dealer_name,
	        'zip_code'=> $zip_code,
	        'phone'=> $phone,
	        'company'=> $company,
	        'city'=> $city,
	        'address2'=> $address2,
	        'address1'=> $address1,
	        'article_no'=> $article_no,
	        'created_by' => $created_by,
	        'created_at' => $created_on,
	        'updated_by' => $updated_by,
	        'updated_at' => $modified_on
	    );

	    $this->db->insert('product_complaint_data',$data);
	    $complaint_id = $this->db->insert_id();

	    if ($complaint_id !='') 
	    {
	    	if (!empty($product_images)) 
		    {
		    	foreach ($product_images as $product_image_str) 
		    	{
		    		$filePath = '';
			      	if ($product_image_str != '') 
			      	{
			      		$image = base64_decode($product_image_str);
			            $image_name = md5(uniqid(rand(), true));
			            // image name generating with random number with 32 characters
			            $filename = $image_name . '.' . 'png';
			            $path = set_realpath('uploads/product_complaint_images');
			            //image uploading folder path
			            file_put_contents($path . $filename, $image);
			            // image is bind and upload to respective folder
			            $filePath = 'uploads/product_complaint_images/'.$filename;

			            $data_img = array(
			            	'complaint_id' => $complaint_id,
					        'product_iamge' => $filePath,
					        'created_by' => $created_by,
					        'created_at' => $created_on,
					        'updated_by' => $updated_by,
					        'updated_at' => $modified_on
			            );

			            $this->db->insert('product_complaint_images',$data_img);
			      	}
		    	}
		    }
		    return true;
	    }else{
	    	return false;
	    }
	}
}