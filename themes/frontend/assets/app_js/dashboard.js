//Document ready event
$(document).ready(function() {
    //Daterange picker for new complaint
    if($('.new_complaint_picker').length) {
        $('.new_complaint_picker input').each(function() {
          $(this).datepicker('clearDates');
        });
        $('.new_complaint_picker').datepicker({});
    }

    //Daterange picker for close cases
    if($('.close_cases_picker').length) {
        $('.close_cases_picker input').each(function() {
          $(this).datepicker('clearDates');
        });
        $('.close_cases_picker').datepicker({});
    }

        

    //Complaint slider
    if ($('.complaint_slider').length) {
        $('.complaint_slider').owlCarousel({
          loop: false,
          items: 5,
          margin: 10,
          nav: true,
          autoplay: false,
          autoplayTimeout: 8500,
          //navigationText : ["prev","next"],
          navText: ["<i class='ti-angle-left'></i>", "<i class='ti-angle-right'></i>"],
          responsive: {
                600: {
                  items: 4
                }
            }
        });
    }

    //Line Chart
    var colors = ['#f54242','#70a331','#f5bf42', '#6b6a57', '#969e9a', '#389688', '#91c3cc', '#088bd1', '#63559e', '#0f0e0f', '#803980', '#380e2e', '#f00c8d', '#b80725', '#145957', '#cdd615', '#11f005', '#660bd6', '#0bd46c', '#c8ded6', '#a3c2d1', '#93a1c9', '#2d1569', '#8f87b0',];
    var multiLineData = {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
            {
                label: 'Bondi 7',
                data: [12, 19, 3, 5, 2, 3, 3, 5, 6, 7, 8, 2],
                borderColor: colors[0],
                backgroundColor: colors[0],
                borderWidth: 2,
                fill: false
            },
            {
                label: 'Challenger ATR 5',
                data: [5, 23, 7, 12, 42, 23, 17, 18, 19, 18, 22, 10],
                borderColor: colors[1],
                backgroundColor: colors[1],
                borderWidth: 2,
                fill: false
            },
            {
                label: 'Torrent 2',
                data: [30, 10, 21, 22, 12, 23, 20, 23, 17, 25, 17, 24],
                borderColor: colors[2],
                backgroundColor: colors[2],
                borderWidth: 2,
                fill: false
            },
            /*{
                label: 'Hupana Flow Wide',
                data: [20, 11, 22, 33, 13, 34, 21, 14, 18, 16, 18, 19],
                borderColor: colors[3],
                backgroundColor: colors[3],
                borderWidth: 2,
                fill: false
            },
            {
                label: 'ORA recovery slide',
                data: [25, 12, 23, 30, 14, 40, 22, 15, 19, 17, 19, 25],
                borderColor: colors[4],
                backgroundColor: colors[4],
                borderWidth: 2,
                fill: false
            },
            {
                label: 'Clifton L',
                data: [15, 13, 27, 35, 15, 18, 23, 9, 25, 18, 20, 21],
                borderColor: colors[5],
                backgroundColor: colors[5],
                borderWidth: 2,
                fill: false
            },
            {
                label: 'Evo Speedgoat',
                data: [10, 14, 35, 36, 20, 37, 24, 17, 21, 10, 21, 22],
                borderColor: colors[6],
                backgroundColor: colors[6],
                borderWidth: 2,
                fill: false
            }, 
            {
                label: 'Carbon X-spe',
                data: [20, 10, 26, 37, 17, 38, 35, 18, 25, 20, 12, 10],
                borderColor: colors[7],
                backgroundColor: colors[7],
                borderWidth: 2,
                fill: false
            }*/
        ]
    };

    var options = {
        responsive: true,
        title: { display: true, text: "Complaint Chart" },
        legend: { position: 'right' },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: "Complaints",
                }
            }],
            // xAxes: [{
            //     ticks: {
            //         beginAtZero: true
            //     },
            //     scaleLabel: {
            //         display: true,
            //         labelString: "Months",
            //     }
            // }]
        },
        tooltips: {
            mode: 'index',
            intersect: true,
        },
        hover: {
            mode: 'nearest',
            intersect: false
        },
        elements: {
            point: {
                radius: 0
            }
        }
    };

    if ($("#linechart-multi").length) {
        var multiLineCanvas = $("#linechart-multi").get(0).getContext("2d");
        var lineChart = new Chart(multiLineCanvas, {
            type: 'line',
            data: multiLineData,
            options: options
        });
    }

    //Close cases datatable
    $('#close_cases').DataTable({
        // "dom": '<"bottom"i>rt<"top"flp><"clear">',
        // "aLengthMenu": [
        //     [5, 10, 15, 25, -1],
        //     [5, 10, 15, 25, "All"]
        // ],
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
    });

    $('#close_cases').each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });

    $('input[type=radio][name=optionsRadios]').change(function(event) {
        $("#radio_button_grid").addClass("hide");
        $("#message_box_grid").removeClass("hide");

        $("#accepted_btn").removeAttr("disabled");
        $("#not_accepted_btn").removeAttr("disabled");
    });
});

//Generate new complaint report
function generateNewComplaintReport() {
    alert('In generate new complaint reports');
}

//Generate close cases report
function generateCloseCasesReport() {
    alert('In generate close cases reports');
}

//Display complaint details
function viewProductDetails(id, visibility) {
    $("#message_box_grid").addClass("hide");
    $("#radio_button_grid").removeClass("hide");
    $('input[type=radio][name=optionsRadios]').prop('checked', false);
    $('#complaint_modal_popup').modal('show');
    $("#accepted_btn").attr("disabled", "disabled");
    $("#not_accepted_btn").attr("disabled", "disabled");
}

//Close cases pdf
function closeCasesPDF() {
    alert('Download Pdf');
}

function showRadioGrid() {
    $("#message_box_grid").addClass("hide");
    $("#radio_button_grid").removeClass("hide");
    $("#accepted_btn").attr("disabled", "disabled");
    $("#not_accepted_btn").attr("disabled", "disabled");
    $('input[type=radio][name=optionsRadios]').prop('checked', false);
}